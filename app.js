import express from "express";
import fs from "fs";

const app = express();
app.use(express.static("mfront"))

app.get("/1", (req, res) => {
    fs.readFile("mfront/pages/page1/page1.html", (err, data) => {
        res.setHeader("Content-Type", "text/html");
        res.send(data);
    })
})

app.get("/2", (req, res) => {
    fs.readFile("mfront/pages/page2/page2.html", (err, data) => {
        res.setHeader("Content-Type", "text/html");
        res.send(data);
    })
})

app.get("/3", (req, res) => {
    fs.readFile("mfront/pages/page3/page3.html", (err, data) => {
        res.setHeader("Content-Type", "text/html");
        res.send(data);
    });
})

app.get("/4", (req, res) => {
    fs.readFile("mfront/pages/page4/page4.html", (err, data) => {
        res.setHeader("Content-Type", "text/html");
        res.send(data);
    });
})

app.get("/5", (req, res) => {
    fs.readFile("mfront/pages/page5/page5.html", (err, data) => {
        res.setHeader("Content-Type", "text/html");
        res.send(data);
    });
})

app.get("/6", (req, res) => {
    fs.readFile("mfront/pages/page6/page6.html", (err, data) => {
        res.setHeader("Content-Type", "text/html");
        res.send(data);
    });
})

app.get("/7", (req, res) => {
    fs.readFile("mfront/pages/page7/page7.html", (err, data) => {
        res.setHeader("Content-Type", "text/html");
        res.send(data);
    });
})

app.get("/8", (req, res) => {
    fs.readFile("mfront/pages/page8/page8.html", (err, data) => {
        res.setHeader("Content-Type", "text/html");
        res.send(data);
    });
})

app.get("/9", (req, res) => {
    fs.readFile("mfront/pages/page9/page9.html", (err, data) => {
        res.setHeader("Content-Type", "text/html");
        res.send(data);
    });
})

app.get("/10", (req, res) => {
    fs.readFile("mfront/pages/page10/page10.html", (err, data) => {
        res.setHeader("Content-Type", "text/html");
        res.send(data);
    });
})

app.get("/11", (req, res) => {
    fs.readFile("mfront/pages/page11/page11.html", (err, data) => {
        res.setHeader("Content-Type", "text/html");
        res.send(data);
    });
});

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
    console.log(`started on ${PORT} ...`);
});

 

