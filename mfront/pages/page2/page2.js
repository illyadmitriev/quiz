'use strict'
let body = document.querySelector('body')
let marksData123 = 0
const content = [
    {
        h: 'Розкладаэмо документи по папках',
        p: 'Перетягуванням покладить зошити у видповидни папки.',
        divContent:
            `<div class="wrapper-books">
                <div><img src="./img2/img1.png"></div>
                <div><img src="./img2/img2.png"></div>
                <div><img src="./img2/img3.png"></div>
                <div><img src="./img2/img4.png"></div>
            </div>          
            <div class="wrapper-folders"> 
                <div>
                    <img src="./img2/img5.png">
                </div>
                <div>
                    <img src="./img2/img6.png">
                </div>
            </div>
            <div class="wrapper-folders__txt"> <p>Украинська мова</p>  <p>Математика</p></div>`,
    },
    {
        h: 'Дизнаемось про файли и папки',
        p: 'Позначте правильне закинчення речення.',
        divContent: 
        `<div class="block-two">
            <p>Кожний малюнак зберигаеться в окремому комп'ютерному документи - ...</p>
        </div>
        <div class="wrapper-two">
            <div class="chekbox-block">
                <form id="formData">
                    <div><input type="radio" name="option" class="radio" value="1" /><p>...презентации.</p></div>
                    <div><input type="radio" name="option" class="radio" value="1" /><p>...папци.</p></div>
                    <div><input type="radio" name="option" class="radio" value="1" /><P>...диску.</p></div>
                    <div><input type="radio" name="option" class="radio trustyRadio" value="1" /><P>...фай//ли.</p></div>
                </form>
            </div>
            <div class="chekbox-img">
                <img src="./img2/chekbox2.png">
            </div>
        </div>
        `
        
    },
    {
        h: 'Дизнаемося, як видкрити зображення',
        p: 'Перетягуванням розташуйте команди видкриття файла з графичним зображенням в правильному порядку.',
        divContent: 
        `<div class="wrapper-three">
            <div class="wrapper-three__left">
                <div><p><span>1.</span>Вибрати папку <span>Мои малюнки</span> або иншу за вказивкою вчитателя.</p></div>
                <div><p><span>2.</span>Виконати команду <span>Зберигти як...</span></p></div>
                <div><p><span>3.</span>Ввести новое им'я файла в поле <span>Им'я файла.</span></p></div>
                <div><p><span>4.</span>Клацнути кнопку <span>Зберагти.</span></p></div>
            </div>
            <div class="wrapper-three__right">
                <img src="./img2/wrapper3.png">
            </div>
        </div>
        `
    },
    {
        h: 'Додаткови завдання',
        p: 'Позначте уси правильни кинцивки речення.',
        divContent: 
        `<div class="wrapper-four">
            <div>
                <p>Папки в комп'ютери використоуваюць для...</p>
            </div>
            <div>
                <form id="formData">
                    <div><input type="checkbox" name="option" class="radio trustyRadio" value="1" /><p>...полегшен//ня пошуку файлив.</p></div>
                    <div><input type="checkbox" name="option" class="radio trustyRadio" value="1" /><p>...впорядкув//ання файлив за певними ознаками.</p></div>
                    <div><input type="checkbox" name="option" class="radio trustyRadio" value="1" /><P>...организаци//и комп'ютерних документив за певною схемою.</p></div>
                </form>
            </div>
        </div>
        <div class="wrapper-four-img">
            <img src="./img2/gif.png">
        </div>
        `
    },
    {
        h: 'Додаткови завдання',
        p: "Позначте властивости, яки мають обидва об'экти: и файл, и папка.",
        divContent:
        `<div class="wrapper-five">
            <div>
                <form id="formData">
                    <div><input type="checkbox" name="option" class="radio" value="1" /><p>Килькисть файлив в об'экти.</p></div>
                    <div><input type="checkbox" name="option" class="radio trustyRadio" value="1" /><p>И//м'я.</p></div>
                    <div><input type="checkbox" name="option" class="radio trustyRadio" value="1" /><p>И//м'я папки, в який знаходиться об'экт.</p></div>
                </form>
            </div>
            <div class="wrapper-five__img">
                <div><img src="./img2/wrapper-five__img.png"></div>
            </div>
        </div>
        `
    },
    {
        h: '',
        p: '',
        divContent: 
        `<div class="results-block">
            <div class="results-block__container">
            <h1>Ваші бали: ${marksData123}</h1>
            </div>
        </div>`

    }
]

let $counter = 0

const wrapperTwo = (div, btnNext, btnPrev) => {

   const $radio = div.getElementsByClassName('radio')
   const $radio_trusty = div.getElementsByClassName('trustyRadio')

   btnNext.addEventListener('click', () => {
    let subMark = 0

    for(let i = 0; i < $radio_trusty.length; i++) {
        if($radio_trusty[i].checked) {
            subMark += $radio_trusty[i].value
            $radio_trusty[i].value = 0
        } else {
            subMark -= 1
        }

    }

        if(subMark > 0 && ($counter === 2 || $counter === 4 || $counter === 5)) {
            marksData123 += 1
        } 
        console.log(marksData123)
    })  
}

const createElement = (html) => {
    const div = document.createElement("div")
    div.classList.add('removed-class')
    div.innerHTML = html.trim()

    const $btnNext = div.querySelector('.next')
    $btnNext.addEventListener('click', next)

    const $btnPrev = div.querySelector('.previous')
    $btnPrev.addEventListener('click', prev)

    if($counter >= 5) {
        $btnNext.disabled = true
        $btnNext.style.color = 'grey'
    } else if($counter <= 0) {
        $btnPrev.disabled = true
        $btnPrev.style.color = 'grey'
    }

    wrapperTwo(div, $btnNext, $btnPrev, marksData123)

    return div
}

const createElementContainer = ($counter, content) => {
    return createElement(`
    <div class="container"> 
        <div class="header">
            <div class="header__h1">
                <h1>Видкриваемо та зберигаемо зображення</h1>
            </div>
        </div>
        <div class="navbar-line">
            <div><img src="./img2/train.png" /></div>
            <div class="navbar-line__txt">
                <div class="navbar-line__txt_one"><img src="./img2/icon.svg" /> <p>${content[$counter].h}</p></div>
                <div class="navbar-line__txt_two"><p>${content[$counter].p}</p></div>
            </div>
        </div>
        <div class="content">
            ${content[$counter].divContent}
        </div>
        <div class="controls">
                <button class="btn btn-primary check">Проверить</button>
                <div class="controls__icons">
                    <button class="fas fa-2x fa-arrow-circle-left previous grey" aria-hidden="true"></button>
                    <button class="fas fa-2x fa-arrow-circle-right next" form="formData" aria-hidden="true"></button>
                </div>
                <button class="btn btn-primary end" style="display: none;">Завершити</button>
        </div>
    </div>
    `)
}

body.appendChild(createElementContainer($counter, content))

let $btnNext = document.querySelector('.next')
let $btnPrev = document.querySelector('.previous')

function next() {
    if($counter > 4) {
        $counter = 5

    } else {
        $counter++
    }

    document.querySelector('.removed-class').remove()
    document.querySelector('body').appendChild(createElementContainer($counter, content))

    
}

function prev() {
    if($counter <= 0) {
        $counter = 0
    } else {
        $counter--
    }
    document.querySelector('.removed-class').remove()
    document.querySelector('body').appendChild(createElementContainer($counter, content))
}

$btnNext.addEventListener('click', next)
$btnPrev.addEventListener('click', prev)


