const questionsRaw = [
    {
        title: "Графічні зображення",
        subtitle: "З'ясовуємо, що таке графічне зображення",
        src: "./img/train.png",
        question: `Встановіть відповідність між наведеними графічними зображення  та їх назвами.`,
        answer1: "3 - Малюнак,&nbsp;&nbsp;  5 - Схема,&nbsp;&nbsp; 1 - Фотографія,&nbsp;&nbsp; 2 - Ескіз,&nbsp;&nbsp; 4 - Креслення",
        answer1Total: "0",
        answer2: "1 - Малюнак,&nbsp;&nbsp;  2 - Схема,&nbsp;&nbsp; 3 - Фотографія,&nbsp;&nbsp; 4 - Ескіз,&nbsp;&nbsp; 5 - Креслення",
        answer2Total: "1",
        answer3: "5 - Малюнак,&nbsp;&nbsp;  3 - Схема,&nbsp;&nbsp; 2 - Фотографія,&nbsp;&nbsp; 4 - Ескіз,&nbsp;&nbsp; 1 - Креслення",
        answer3Total: '0',
        correctAnswer: 1,
    },
    {
        title: "Графічні зображення",
        subtitle: "З'ясовуємо, що таке графічне зображення",
        src: "./img/train.png",
        question:
            "2. Розгляньте наведені об'єкти. Позначте ті, за допомогою яких можна створаюти графічні зображення.",
        answer1: "1, 3, 5, 6, 7",
        answer1Total: "0",
        answer2: "2, 4",
        answer2Total: "0",
        answer3: "1, 3, 6, 7",
        answer3Total: "0",
        answer4: "Все",
        answer4Total: "1",
        correctAnswer: 3,
    },
    {
        title: "Графічні зображення",
        subtitle: "Згадуємо алгоритми з повсякденного життя",
        src: "./img/logic.png",
        question:
            "3. Розгляньте малюнки. Підписи під ними переплутано. З'ясуйте, до якого виду належать подані зображення.",
        answer1: "1 - Малюнок,&nbsp;&nbsp;  2 - Фотографія,&nbsp;&nbsp; 3 - Схема,&nbsp;&nbsp;  4 - Креслення,&nbsp;&nbsp; 5 - Ескіз",
        answer1Total: "0",
        answer2: "1 - Фотографія,&nbsp;&nbsp;  2 - Малюнок,&nbsp;&nbsp; 3 - Креслення,&nbsp;&nbsp;  4 - Схема,&nbsp;&nbsp; 5 - Ескіз",
        answer2Total: "1",
        answer3: "1 - Ескіз,&nbsp;&nbsp;  2 - Креслення,&nbsp;&nbsp; 3 - Схема,&nbsp;&nbsp;  4 - Фотографія,&nbsp;&nbsp; 5 - Малюнок",
        answer3Total: "0",
        correctAnswer: 1,
    },
    {
        title: "Графічні зображення",
        subtitle: "Додаткові завдання",
        src: "./img/work-together.png",
        question:
            "Розгляньте малюнки. Підписи під ними переплутано. Виправте помилки.",
        correctAnswerSequence: [5, 4, 3, 2, 1, 0],
        questionSequence: [    
            `<img src="img/page6_23.png" alt="water can3" />`,
            `<img src="img/page6_22.png" alt="water can3" />`,
            `<img src="img/page6_21.png" alt="water can1" />`,
            `<img src="img/page6_20.png" alt="water can2" />`,
            `<img src="img/page6_19.png" alt="water can3" />`,
            `<img src="img/page6_18.png" alt="water can3" />`,
        ],
    },
    {
        title: "Графічні зображення",
        subtitle: "Логічні задачі",
        src: "./img/work-together.png",
        question:  "Для однієї секунди мультфільму художник створює 8 кадрів. Скільки потрібно створити кадрів для мультфільму, що триває 5 секунд?",
        answer1: "35",
        answer1Total: "0",
        answer2: "40",
        answer2Total: "0",
        answer3: "45",
        answer3Total: "1",
        correctAnswer: 1,
    },
];

const contentRaw = [
    {
        content: `
          <div class="page6-content">
            <div> <span>1</span> <img src="img/page6_1.png" alt="step1"></div>
            <div> <span>2</span> <img src="img/page6_2.png" alt="step2"></div>
            <div> <span>3</span> <img src="img/page6_3.png" alt="step3"></div>
            <div> <span>4</span> <img src="img/page6_4.png" alt="step4"></div>
            <div> <span>5</span> <img src="img/page6_5.png" alt="step5"></div>
          </div>
        `,
    },
    {
        content: `
          <div class="word-container">
            <div class="word-img word-img__page6">
              <div><img src="img/page6_6.png" alt="guys"> <div>1</div> </div> 
              <div> <img src="img/page6_7.png" alt="guys"> <div>2</div> </div> 
              <div> <img src="img/page6_8.png" alt="guys"> <div>3</div> </div>  
              <div> <img src="img/page6_9.png" alt="guys"> <div>4</div> </div> 
              <div> <img src="img/page6_10.png" alt="guys"> <div>5</div> </div> 
              <div> <img src="img/page6_11.png" alt="guys"> <div>6</div> </div> 
              <div> <img src="img/page6_12.png" alt="guys"> <div>7</div> </div> 
            </div>
          </div>
        `,
    },
    {
        content: `
          <div class="str3__page6">
            <div> <img src="img/page6_13.png" alt="teacher"> <span>1</span> </div>
            <div> <img src="img/page6_14.png" alt="teacher"> <span>2</span></div>
            <div> <img src="img/page6_15.png" alt="teacher"> <span>3</span> </div>
            <div> <img src="img/page6_16.png" alt="teacher"> <span>4</span> </div>
            <div> <img src="img/page6_17.png" alt="teacher"> <span>5</span> </div>
          </div>
        `,
    },
    {
        content: `
            <div class="str4__page6">
                <p>1: Схема</p>
                <p>2: Анімація</p>
                <p>3: Креслення</p>
                <p>4: Малюнак</p>
                <p>5: Діаграма</p>
                <p>6: Фотографія</p>
            </div>
        `,
    },
    {
        content: `
        <div class="str5__page6">
            <div><img src="img/page6_24.png" /></div>
        <div>
        `,
    },

];

const setupDrag = () => {
    let dragged, $draggablezone, $dropzone;

    document.addEventListener(
        "dragstart",
        (event) => {
            const $el = event.target;

            $draggablezone = $el.closest(".draggablezone");
            $dropzone = $el.closest(".dropzone");
            if (!$draggablezone || !$dropzone) return;

            dragged = $el;
            $dropzone.style.opacity = 0.5;
        },
        false
    );
    document.addEventListener(
        "dragend",
        (event) => {
            $dropzone.style.opacity = "";
            $dropzone = $draggablezone = null;
        },
        false
    );
    document.addEventListener(
        "drag",
        (event) => {
            if (!$draggablezone || !$dropzone) return;

            const $el = event.target;
            console.log(event);
            const {clientX, clientY} = event;

            [...$draggablezone.querySelectorAll(".dropzone")].map((v) => {
                const bbox = v.getBoundingClientRect();
                if (
                    bbox.x < clientX &&
                    bbox.y < clientY &&
                    bbox.x + bbox.width > clientX &&
                    bbox.y + bbox.height > clientY
                ) {
                    if (v === $dropzone) return;

                    if (clientY - bbox.y > bbox.height / 2) {
                        v.before($dropzone);
                    } else {
                        v.after($dropzone);
                    }
                }
            });

            const a = [...$draggablezone.querySelectorAll(".dropzone")].map((v) =>
                decode(v.dataset.data)
            );
            if ($draggablezone._onChangeOrder) $draggablezone._onChangeOrder(a);
        },
        false
    );
    document.addEventListener(
        "dragover",
        (event) => {
            event.preventDefault();
        },
        false
    );

    document.addEventListener(
        "dragenter",
        (event) => {
            const $el = event.target;

            const $draggablezone = $el.closest(".draggablezone");
            const $dropzone = $el.closest(".dropzone");
            if (!$draggablezone || !$dropzone) return;

            $dropzone.style.background = "purple";
        },
        false
    );

    document.addEventListener(
        "dragleave",
        (event) => {
            if (event.target.className === "dropzone") {
                event.target.style.background = "";
            }
        },
        false
    );

    document.addEventListener(
        "drop",
        (event) => {
            event.preventDefault();
            if (event.target.className === "dropzone") {
                event.target.style.background = "";
            }
        },
        false
    );
}

setupDrag();

const createElement = (html) => {
    const div = document.createElement("div");
    div.innerHTML = html.trim();
    return div.firstChild;
};

const encode = (text) =>
    text
        .split("")
        .map((c) => c.charCodeAt().toString(16))
        .join("_");
const decode = (text) =>
    text
        .split("_")
        .map((c) => String.fromCharCode(parseInt(c, 16)))
        .join("");

function QuestionMng(questionsRaw, $mountTo) {
    let question;
    let index = 0;
    let showInfoSelectAnswer = false;
    let showResult = false;
    let questions = [];
    let close, window;
    const init = () => {
        questions = questionsRaw.map((q) => {
            const question = q.question;
            const title = q.title;
            const subtitle = q.subtitle;
            const src = q.src;
            const close = q.close;
            const window = q.window;
            const answers = [];
            for (let i = 1, answer; (answer = q[`answer${i}`]); i++) {
                answers.push({
                    src,
                    title,
                    subtitle,
                    answer,
                    close,
                    window,
                    score: +q[`answer${i}Total`],
                });
            }

            const questionSequence = q.questionSequence
                ? [...q.questionSequence]
                : [];
            const correctAnswerSequence = (q.correctAnswerSequence || []).map(
                (i) => questionSequence[i]
            );

            return {
                check() {
                    if (this.correctAnswerSequence.length)
                        return this.questionSequenceTmp.every(
                            (v, i) => v === this.correctAnswerSequence[i]
                        );

                    return +this.answerIndex === +this.correctAnswer;
                },

                src,
                title,
                subtitle,
                question,
                answers,
                close,
                window,
                correctAnswer: q.correctAnswer,
                correctAnswerSequence: correctAnswerSequence,

                questionSequence: questionSequence,
                questionSequenceTmp: [...questionSequence],
                answerIndex: -1,
                showTrueAnswer: false,
            };
        });

        index = 0;
        showResult = false;
        showInfoSelectAnswer = false;
    }

    init();
    const $container = createElement(`
	<div class="quiz-container">
		<div class="fl_dc questions-container" >
      <div class="fl_dc quiz-container-wrapper"></div>
		 
			<div class="window info-select-answer fade-in" role="alert" style="display: none;" >
                <img src="img/2_small.png" alt="smile">
                Выберите ответ! 
                <button type="button" class="btn cancel btn-danger">&#10008;</button>
			</div>
			<div class="window info-answer-invalid fade-in" role="alert" style="display: none;" >
                <img src="img/2_small.png" alt="smile">
                Ответ не правильный!
			</div>
			<div class="window info-answer-valid fade-in" role="alert" style="display: none;" >
                Ответ правильный! 
			</div>
		 
			<div class="controls">
				<button class="btn btn-primary check">Проверить</button>
				<div class="controls__icons">
					<i class="fas fa-2x fa-arrow-circle-left previous grey"></i>
					<i class="fas fa-2x fa-arrow-circle-right next"></i>
				</div>
				<button class="btn btn-primary end" style="display: none;" >Завершити</button>
			</div>
		</div>
		
		<div class="fl_dc result-container">
			<h1 class="true-answers-rate">Правильних відповідей :${0}</h1>
			<h1 class="final-score">Ваші бали:${0}</h1>
			<button class="restart">Рестарт</button>
		</div>
	</div>
	`);

    const $quizContainerWrapper = $container.querySelector(
        ".quiz-container-wrapper"
    );
    const $body = $container.querySelector("body")
    const $infoSelectAnswer = $container.querySelector(".info-select-answer");
    const $infoAnswerInvalid = $container.querySelector(".info-answer-invalid");
    const $infoAnswerValid = $container.querySelector(".info-answer-valid");
    const $prevBtn = $container.querySelector(".previous");
    const $nextBtn = $container.querySelector(".next");
    const $endBtn = $container.querySelector(".end");
    const $restartBtn = $container.querySelector(".restart");
    const $questionsContainer = $container.querySelector(".questions-container");
    const $resultContainer = $container.querySelector(".result-container");
    const $finalScore = $container.querySelector(".final-score");
    const $trueAnswersRate = $container.querySelector(".true-answers-rate");
    const $close = $container.querySelector(".cancel")
    const $window = $container.querySelector(".window")

    $mountTo.appendChild($container);
    const render = () => {
        question = questions[index];
        const trueAnswer = question.answers.find(
            (a) => a.score === Math.max(...question.answers.map((a) => a.score))
        );
        const html =
            `
    <div id="title" class="title">
    ${question.title}
    </div>
    <div id="question" class="question">
        <img src=${question.src} alt="some-img">
        <div>
            <div class="question__subtitle">
                <img src="./img/icon.svg" alt="icon">
                ${question.subtitle}
            </div>
            <div class="question__main">
                ${question.question}
            </div>
        </div>
    </div>
	
	${
                !question.questionSequenceTmp.length
                    ? ""
                    : `
		<div class="draggablezone draggablezone__page6" >
		${question.questionSequenceTmp
                        .map(
                            (q) => `
				<div class="dropzone" data-data="${encode(q)}" >
					<div id="draggable" draggable="true" ondragstart="event.dataTransfer.setData('text/plain',null)">
						${q}
					</div>
				</div>		
			`
                        )
                        .join("")}
		</div>
	`
            }
	
    <div>
     ${contentRaw[index].content}
    </div>
		` +
            question.answers
                .map(
                    (a, i) => `
                <div class="options-container options-container__page6">
                    <label class="option">
                        <input type="radio" name="option" value="1" data-action="input-option-answer" data-id="${i}" 
                        ${question.answerIndex === i ? "checked" : ""} />
                        <span class="option1">${a.answer}</span>
                    </label>
                </div>
		`
                )
                .join("");

        $quizContainerWrapper.innerHTML = html;
        const $tmp = $quizContainerWrapper.querySelector(".draggablezone");
        if ($tmp)
            $tmp._onChangeOrder = (a) => {
                question.questionSequenceTmp = a;
            };

        $questionsContainer.style.display = "";
        $resultContainer.style.display = "none";
        $infoSelectAnswer.style.display = "none";
        $infoAnswerValid.style.display = "none";
        $infoAnswerInvalid.style.display = "none";
        $nextBtn.style.display = "";
        $endBtn.style.display = "none";

        $prevBtn.classList.toggle("grey", index === 0)


        if (showInfoSelectAnswer) {
            $infoSelectAnswer.style.display = "flex";
            $close.addEventListener("click", () => {
                $window.style.visibility = "hidden"
            })
        }

        if (question.showCheckAnswer) {
            if (question.correctAnswerSequence.length) {
                if (question.check()) {
                    $infoAnswerValid.style.display = "flex";
                    setTimeout(() => {
                        $infoAnswerValid.style.display = "none";
                    }, 1500)
                } else {
                    $infoAnswerInvalid.style.display = "flex";
                    setTimeout(() => {
                        $infoAnswerInvalid.style.display = "none";
                    }, 1500)
                }
            } else if (question.answerIndex < 0) {
                $infoSelectAnswer.style.display = "flex";
                setTimeout(() => {
                    $infoSelectAnswer.style.display = "none";
                }, 1500)
            } else if (question.check()) {
                $infoAnswerValid.style.display = "flex";
                setTimeout(() => {
                    $infoAnswerValid.style.display = "none";
                }, 1500)
            } else {
                $infoAnswerInvalid.style.display = "flex";
                setTimeout(() => {
                    $infoAnswerInvalid.style.opacity = "0";
                }, 1500)
            }
        }

        console.log(index, questions.length)
        if (index + 1 >= questions.length) {
            $nextBtn.style.display = "none";
            $endBtn.style.display = "";
        }

        if (showResult) {
            $questionsContainer.style.display = "none";
            $resultContainer.style.display = "";

            const numCorrectAnswers = questions.reduce(
                (s, q) => s + (q.check() | 0),
                0
            );
            const score = questions.reduce(
                (s, q) =>
                    s +
                    ((q.answers[q.answerIndex] && q.answers[q.answerIndex].score) | 0),
                0
            );
            $trueAnswersRate.textContent = `Правильних відповідей: ${numCorrectAnswers}/${questions.length}`;
            $finalScore.textContent = `Ваші бали: ${score}`;
        }
    }

    render();

    const next = () => {
        if (question.answerIndex < 0 && !question.correctAnswerSequence.length) {
            showInfoSelectAnswer = true;
            render();
            return;
        }

        if (index >= questions.length - 1) {
            showResult = true;
            render();
            return;
        }

        index++;
        render();
    }


    const prev = () => {
        if (!index) {
            return;
        }
        index--;
        showInfoSelectAnswer = false;
        render();
    }

    function check() {
        question.showCheckAnswer = true;
        render();
    }

    function end() {
        next();
    }

    function restart() {
        console.log("restart");
        init();
        render();
    }

    function changeAnswer(id) {
        question.answerIndex = +id;
        showInfoSelectAnswer = false;
        question.showCheckAnswer = false;
        render();
    }

    $container.addEventListener("change", ({target}) => {
        const {dataset} = target;
        switch (dataset.action) {
            case "input-option-answer":
                changeAnswer(dataset.id);
                break;
        }2
    });
    $prevBtn.addEventListener("click", prev);
    $nextBtn.addEventListener("click", next);
    $endBtn.addEventListener("click", end);
    $restartBtn.addEventListener("click", restart);
    $container.querySelector(".check").addEventListener("click", check);
}


new QuestionMng(questionsRaw, document.querySelector("body"));

